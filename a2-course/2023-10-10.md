# 2023-10-10

## Neue Wörter

| Deutsch     | Englisch                   |
| ----------- | -------------------------- |
| indem       | by                         |
| draft       | der Entwurf, die Entwürfen |
| gibt        | exist                      |
| fangen      | catch                      |
| nutzlos     | useless                    |
| notwendig   | necessary                  |
| notwendiger | more necessary             |
| komparative | comparative                |
| als         | than                       |
| verrückt    | crazy                      |
| gleich      | equal / even               |
| genauso     | equally / just as          |
| beide       | both                       |
| weder       | neither                    |
| Vergleich   | comparison                 |
| Werbung     | advertisement              |

## Notizen

Ich schütze mich vor Fake News, indem ich offline bleibe.

geben+es  
Es gibt <ins>Rabatt</ins> (+ akkusativ).

| Nominativ   | Akkusativ   | Dativ        |
| ----------- | ----------- | ------------ |
| der Rabatt  | den Rabatt  | dem Rabatt   |
| die Meinung | die Meinung | der Meinung  |
| das Handy   | das Handy   | dem Handy    |
| die Kinder  | die Kinder  | den Kinder+n |

Die <ins>Kinder</ins> (Subjekt: Nominativ) spielen.  
Die Kinder fangen (Verb: Akkusativ) den <ins>Ball</ins> (Objekt: Akkusativ).  
Die Kinder (Subjekt: Nominativ) spielen mit (+ Dativ) <ins>dem Ball</ins> (Dativ).  
Die Kinder sehen (Objekt: Akkusativ) einen Bär mit (+ Dativ) <ins>dem Fernglas</ins> (Dativ).

Peter ist <ins>lustiger</ins> als Paul.  
Max ist am lustigsten.
Max ist der Lustigste (<-- Substantiv).  
Der Lustigste heißt Max.

Was isst ... zum Frühstück?  
Was frühstückt ...?  
Frühstückt ... mehr als ...?  
Wer macht mehr Sport, ... oder ...?  
Wie oft macht ... Sport?  
P.B. joggt <ins>genauso</ins> viel <ins>wie</ins> P.D.  
Beide joggen gleich viel.  
<ins>Weder</ins> ... <ins>noch</ins> isst Quark zum Frühstück.

## Bilder

![Bild 1](images/2023-10-10-1.jpg)
![Bild 2](images/2023-10-10-2.jpg)
![Bild 3](images/2023-10-10-3.jpg)
![Bild 4](images/2023-10-10-4.jpg)
![Bild 5](images/2023-10-10-5.jpg)
![Bild 6](images/2023-10-10-6.jpg)
![Bild 7](images/2023-10-10-7.jpg)

## Hausaufgabe

Übung 1, 2 und 4.

### Übung 1.

**Was kann man lesen, hören oder sehen? Sortieren Sie.**

Buch, Dokumentation, Fernsehen, Film, Illustrierte, Konzert, Krimi, Kultursendung, Nachrichten, Radio, Reportage, Sendung, Sportsendung, Talkshow, Werbung, Zeitschrift, Zeitung

| Man liest es: | Man sieht es: | Man hört es: |
| ------------- | ------------- | ------------ |
| Buch          | Fernsehen     | Konzert      |
| Dokumentation | Film          | Radio        |
| Krimi         | Illustrierte  |
| Nachrichten   | Kultursendung |
| Reportage     | Sendung       |
| Werbung       | Sportsendung  |
| Zeitschrift   | Talkshow      |
| Zeitung       |               |

### Übung 2.

**Interviewen Sie Ihren Nachbarn / Ihre Nachbarin.**

Aktivitäten: Zeitschriften, Romane, Zeitungen, Comics, Nachrichten, Liebesfilme, Talkshows, Dokumentationen, Komödien, Konzerte, Sportsendungen, Werbung.

Vergleich: Oft, öfter, am häufigsten, gern, lieber, am liebsten, gut, besser, am besten, viel, mehr, am meisten.

Aktionen: Mögen, gefallen, lesen, sehen, hören.

> Ich bin introvertiert. Ich kann meinen Nachbarn nicht interviewen, so ich interviewe meine Frau.

![Sara Harka <3](https://harka.com/img/sara-harka.jpg)

Daniel: Du, Sara!  
Sara: Ja?  
D: Ich muss dich interviewen.  
S: Na dann!  
D: Wie oft liest du Zeitschriften oder Comics?  
S: Ich lese nie Zeitschriften oder Comics.  
D: Liest du öfter Romane, Dokumentationen oder Zeitungen?  
S: Ich lese häufiger Dokumentationen als Zeitungen und Romane am Öftesten.  
D: Siehst du gern Talkshows?  
S: Nein, Talkshows sehe ich nicht Gern.  
D: Gefällst du Liebesfilme oder Komödien?  
S: Ich mag Komödien mehr als Liebesfilme.  
D: Wie viel Nachrichten und Sportsendungen siehst du?  
S: Ich sehe keine Nachrichten und Sportsendungen.  
D: Siehst du gern Werbung?  
S: Nein, das mache ich nicht.  
D: Aber hörst du gern Konzerte?  
S: Jaaa! Lass uns gehen! 🤘

### Übung 4.

**Bilden Sie Sätze.**

1. viel fernsehen / Tom / Andi / Peter  
   _Tom sieht viel fern. Andi sieht mehr fern. Peter sieht am meisten fern._

1. gut Deutsch sprechen / Sarah / Mia / Frank  
   _Sarah spricht gut Deutsch. Mia spricht besser Deutsch. Frank spricht am besten Deutsch._

1. viel trinken / Kaffee / Tee / Limonade (ich)  
   _Ich trinke viel Kaffee. Ich trinke mehr Tee. Ich trinke am meisten Limonade._

1. oft Radio hören / wir / ihr / du  
   _Wir hören oft Radio. Ihr hört häufiger Radio. Du hörst am häufigsten Radio._

1. gut Ski fahren / ich / mein Bruder / meine Tante  
   _Ich fahre gut Ski. Mein Bruder fährt besser Ski. Meine Tante fährt am besten Ski._

1. viel lesen / du / deine Schwester / dein Vater  
   _Du liest viel. Deine Schwester liest mehr. Dein Vater liest am meisten._

1. viel essen / Käse / Wurst / Brot (du)  
   _Du isst viel Käse. Du isst mehr Wurst. Du isst am meisten Brot._

1. gern tanzen / Herr Beck / Martin / Ella  
   _Herr Beck tanzt gern. Martin tanzt lieber. Ella tanzt am liebsten._

1. gut kochen / ich / er / Margit  
   _Ich koche gut. Er kochst besser. Margit kocht am besten._

1. häufig ins Kino gehen / wir / ihr / sie (PL)  
   _Wir gehen häufig ins Kino. Ihr geht häufiger ins Kino. Sie gehen am häufigsten ins Kino._

1. gern Musik hören / Hans / Karl / ich  
   _Hans hört gern Musik. Karl hört leiber Musik. Ich höre am liebsten Musik._
