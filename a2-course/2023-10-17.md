# 2023-10-10

## Neue Wörter

| Deutsch            | Englisch                       |
| ------------------ | ------------------------------ |
| die Urafe,-n       | survey                         |
| Aufwachen          | waking up                      |
| läufe              | run                            |
| selbstverständlich | self-evident, natural, obvious |
| vorstellen         | imagine                        |
| die Meinung,-en    | opinions                       |
| meinen             | to think/believe               |
| Hauptsatz          | main sentence / clause         |
| Nebensatz          | subordinate sentence / clause  |

## Notizen

### Fragen

Was gefällt dir besser, Talkshows oder Liebesfilmen?  
Welchen Roman mögst du am liebsten?  
Liest du Österreichische Zeitungen?  
Siehst du häufiger Dokumentationen oder Komödien?  
Hören Sie oft Talkshows?

### Alltag und Medien

Ein Leben ohne Medien? Das geht nicht. Schon zum Aufwachen brauche ich Musik. Wenn die Kaffeemaschine läuft, schalte ich den Computer an und beantworte meine Mails. Beim Frühstück lese ich die aktuellen Nachrichten. Nein, nicht in der Zeitung. Das ist ja alles schon von gestern. Ich lese die Nachrichten im Internet. Die sind ganz aktuell. Im Büro habe ich natürlich auch einen Computer. Das ist heute selbstverständlich, denn ich bin Architektin und wir arbeiten mit ganz neuen Programmen. Ohne Internet und E-Mail kann ich mir den Beruf gar nicht mehr vorstellen. Und mein Handy ist genauso wichtig wie mein Auto. Ich bin viel unterwegs und man muss mich immer erreichen können. Wenn ich abends zu Hause bin, will ich Ruhe haben. Kein Telefon, kein Handy. Ich höre dann Musik, lese oder ich sehe mir einen Film an. Am Wochenende treffe ich mich mit Freunden oder ich gehe ins Kino.

### Die Umfrage

A) Wie viele Stunden surfst du täglich im Internet?  
B) Wie viele Stunden hörst du am Tag Musik?  
C) Wie viele Social Media-Kanäle hast du?  
D) Wie viele Bücher liest du pro Jahr?

|         | A   | B   | C   | D   |
| ------- | --- | --- | --- | --- |
| Botond  | 3   | 1   | 4   | 10  |
| Zsófi   | 16  | 3   | 5   | 3   |
| Daniel  | 8+3 | 1   | 6   | 5   |
| Magda   | 2   | 4   | 2   | 15  |
| Chiara  | 4   | 3   | 5   | 10  |
| Maria   | 5   | 6   | 3   | 8   |
| Pratima | 2   | 3   | 3   | 1   |

Ich habe <ins>genau</ins> <ins>so</ins> viel SM-K wie ... .  
Zs hört <ins>genau</ins> <ins>so</ins> viel Musik wie ... .

### Die Meinungen

Seite 24. 4. a)

1. Ich finde, dass Kinder zu viel fernsehen.
2. Ich meine, dass Zeitungen informiert besser als Fernsehen.
3. Ich denke, dass wir Bucher brauchen, aber wir können die auch digital lesen.
4. Ich finde, dass das Internet viel mehr als andere Medien bietet.

|                      |                                                   |
| -------------------- | ------------------------------------------------- |
| <ins>Ich finde</ins> | dass Kinder (nicht) zu viel <ins>fernsehen</ins>. |
| Hauptsatz            | Nebensatz                                         |

## Bilder

![Bild 1](images/2023-10-17-1.jpg)  
![Bild 2](images/2023-10-17-2.jpg)  
![Bild 3](images/2023-10-17-3.jpg)  
![Bild 4](images/2023-10-17-4.jpg)  
![Bild 5](images/2023-10-17-5.jpg)

## Hausaufgabe

### 16. Mediennutzung. Screiben Sie Sätze.

1. Ich finde es schlecht, dass die Internetverbindung in Kleinstädten oft nicht gut ist.
1. Ich finde, dass man muss immer erreichbar sein.
1. Ich finde es gut, dass man kann Vokabeln auch mit einer App lernen.

### 17. Das Leben in Österreich. Screiben Sie Sätze.

1. Ich finde, dass die Zugfahrkarten in Österreich sehr teuer sind.
1. Ich denke, dass der Winter sehr kalt ist.
1. Ich finde es gut, dass man am Wochenende viel machen kann.
1. Ich finde es schlecht, dass man nur schwer eine Wohnung findet.
1. Ich denke nicht, dass alle Menschen ein Auto haben.

### Schreibt einen kleinen Text.

Ich sehe, dass ein Bus kommt und ein Mann mit einer dunklen Sonnenbrille aussteigt. Ich höre, dass er telefoniert und dass er lacht. Ich erkenne, dass er eine Waffe unter dem Pullover trägt. Ich glaube, dass er wird etwas schlecht machen. Ich vermute, dass er wird vielleicht eine Bank ausrauben. Ich sehe, dass er ein anderen Mann trifft. Ich höre, dass sie leise sprechen. Ich glaube, dass sie Pläne machen. Ich sehe, dass sie zum Schwimmbad-Kasse gehen. Ich rufe, dass "Hände hoch!". Ich höre, dass sie schreien. Ich sehe, dass sie die Waffen fallen lassen. Ich erkenne, dass die nur Wasserpistolen sind. Ich denke, dass ich ein Idiot bin.
